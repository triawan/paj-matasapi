-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2021 at 12:41 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paj-matasapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `id` int(20) NOT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `nik` varchar(30) NOT NULL,
  `prefix_title` varchar(30) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `suffix_title` varchar(30) DEFAULT NULL,
  `phonetic_name` varchar(30) DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `place_of_birth` varchar(40) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address_id` int(20) DEFAULT NULL,
  `contact_id` int(20) DEFAULT NULL,
  `body_weight` decimal(18,1) DEFAULT NULL,
  `body_height` decimal(18,1) DEFAULT NULL,
  `blood_type_id` int(20) DEFAULT NULL,
  `marital_status_id` int(20) DEFAULT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `emergency_address` varchar(100) DEFAULT NULL,
  `family_card_id` int(20) DEFAULT NULL,
  `insurance_id` int(20) DEFAULT NULL,
  `bank_account_id` int(20) DEFAULT NULL,
  `npwp` varchar(30) DEFAULT NULL,
  `photo_source` varchar(120) DEFAULT NULL,
  `personal_website` varchar(80) DEFAULT NULL,
  `primary_email` varchar(80) DEFAULT NULL,
  `secondary_email` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `empeducations`
--

CREATE TABLE `empeducations` (
  `id` int(20) NOT NULL,
  `Description` varchar(200) NOT NULL,
  `EmpEducationTypeId` int(20) NOT NULL,
  `FinishDate` date DEFAULT NULL,
  `IsDeleted` bit(1) DEFAULT NULL,
  `Name` varchar(120) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EmployeeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `empeducationtypes`
--

CREATE TABLE `empeducationtypes` (
  `id` int(20) NOT NULL,
  `IsDeleted` bit(1) NOT NULL,
  `Name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `marital_status`
--

CREATE TABLE `marital_status` (
  `id` int(20) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `name_id` varchar(80) NOT NULL,
  `name_en` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `id` int(20) NOT NULL,
  `nik` varchar(30) NOT NULL,
  `prefix_title` varchar(30) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `suffix_title` varchar(30) DEFAULT NULL,
  `phonetic_name` varchar(30) DEFAULT NULL,
  `gender_id` tinyint(1) DEFAULT NULL,
  `place_of_birth` varchar(40) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `address_id` int(20) DEFAULT NULL,
  `contact_id` int(20) DEFAULT NULL,
  `body_weight` decimal(18,1) DEFAULT NULL,
  `body_height` decimal(18,1) DEFAULT NULL,
  `blood_type_id` int(20) DEFAULT NULL,
  `marital_status_id` int(20) DEFAULT NULL,
  `emergency_name` varchar(50) DEFAULT NULL,
  `emergency_contact` varchar(15) DEFAULT NULL,
  `emergency_address` varchar(100) DEFAULT NULL,
  `family_card_id` int(20) DEFAULT NULL,
  `insurance_id` int(20) DEFAULT NULL,
  `bank_account_id` int(20) DEFAULT NULL,
  `npwp` varchar(30) DEFAULT NULL,
  `photo_source` varchar(120) DEFAULT NULL,
  `personal_website` varchar(80) DEFAULT NULL,
  `primary_email` varchar(80) DEFAULT NULL,
  `secondary_email` varchar(80) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `person`
--

INSERT INTO `person` (`id`, `nik`, `prefix_title`, `first_name`, `middle_name`, `last_name`, `suffix_title`, `phonetic_name`, `gender_id`, `place_of_birth`, `date_of_birth`, `address_id`, `contact_id`, `body_weight`, `body_height`, `blood_type_id`, `marital_status_id`, `emergency_name`, `emergency_contact`, `emergency_address`, `family_card_id`, `insurance_id`, `bank_account_id`, `npwp`, `photo_source`, `personal_website`, `primary_email`, `secondary_email`, `is_deleted`, `updated_at`) VALUES
(1, '3305201010670001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-25 08:51:49'),
(2, '3305201010670002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-25 08:58:04'),
(3, '3305201010670003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-25 08:57:03'),
(4, '3305201010670004', 'Dr', 'Agus', NULL, 'Mulyana', 'S.T., M.T', NULL, 1, 'Bandung', '0000-00-00', 2, 2, '70.0', '160.0', 1, 2, 'Sopian', NULL, 'Jalan Unikom Raya Nomor 4', 340303039, 342342, 234234, 'ABC-2324', NULL, NULL, 'agus@gmail.com', NULL, NULL, '2021-11-25 08:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `ref_gender`
--

CREATE TABLE `ref_gender` (
  `id` tinyint(1) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `name_id` varchar(80) NOT NULL,
  `name_en` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `empeducations`
--
ALTER TABLE `empeducations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `empeducationtypes`
--
ALTER TABLE `empeducationtypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `marital_status`
--
ALTER TABLE `marital_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nik` (`nik`);

--
-- Indexes for table `ref_gender`
--
ALTER TABLE `ref_gender`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

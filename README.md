<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h2 align="center">Yii 2 Basic Project Matasapi API GATEWAY</h2>
    <br>
</p>


Pembuatan layanan API GATEWAY untuk project Mata Sapi

Tim CSN :

Agus Mulyana - G6601211014
<br>
Defiana Arnaldy - G6601211005
<br>
Sopian Alviana - G6601211024
<br>
Triawan Adi Cahyanto - G6601211016

FITUR LAYANAN
-------------

### Url untuk akses layanan API untuk data person
~~~
http://localhost/paj-matasapi/web/person
~~~
### Akses layanan API yang tersedia
~~~
GET /person: list all person page by page;
HEAD /person: show the overview information of person listing;
POST /person/create: create a new person;
GET /person/123: return the details of the person 123;
HEAD /person/123: show the overview information of person 123;
PATCH /person/123 and PUT /person/123: update the person 123;
DELETE /person/123: delete the person 123;
OPTIONS /person: show the supported verbs regarding endpoint /person;
OPTIONS /person/123: show the supported verbs regarding endpoint /person/123.
~~~
DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.6.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
composer create-project --prefer-dist yiisoft/yii2-app-basic paj-matasapi
~~~

Now you should be able to access the application through the following URL, assuming `paj-matasapi` is the directory
directly under the Web root.

~~~
http://localhost/paj-matasapi/web/
~~~

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/paj-matasapi/web/
~~~

CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=paj-matasapi',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```
Terdapat db yang merepresentasikan layanan api, yaitu:
```
paj-matasapi.sql
```
UJICOBA
-------
### Akses API untuk GET PERSON
Penampakan ujicoba ada pada gambar berikut ini:

![](https://gitlab.com/triawan/paj-matasapi/-/raw/main/assets/sample-api.PNG)
<figcaption>Gambar Ujicoba API di paj-matasapi (api gateway)</figcaption>
<br>


### Test fitur GET layanan API dengan POSTMAN
Penampakan ujicoba GET layanan API dengan postman:

![](https://gitlab.com/triawan/paj-matasapi/-/raw/main/assets/GET.gif)
<figcaption>Gambar Ujicoba fitur GET dengan postman</figcaption>

### Ujicoba layanan POST layanan API dengan POSTMAN
Penampakannya adalah sebagai berikut:

![](https://gitlab.com/triawan/paj-matasapi/-/raw/main/assets/POST.gif)
<figcaption>Gambar Ujicoba fitur POST dengan postman</figcaption>

### Ujicoba layanan PUT layanan API dengan POSTMAN
Penampakannya adalah sebagai berikut:

![](https://gitlab.com/triawan/paj-matasapi/-/raw/main/assets/PUT.gif)
<figcaption>Gambar Ujicoba fitur PUT dengan postman</figcaption>

### Ujicoba layanan DELETE layanan API dengan POSTMAN
Penampakannya adalah sebagai berikut:

![](https://gitlab.com/triawan/paj-matasapi/-/raw/main/assets/DELETE.gif)
<figcaption>Gambar Ujicoba fitur DELETE dengan postman</figcaption>
<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "marital_status".
 *
 * @property int $id
 * @property bool $is_deleted
 * @property string $name_id
 * @property string $name_en
 */
class Marital_status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marital_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name_id', 'name_en'], 'required'],
            [['id'], 'integer'],
            [['is_deleted'], 'boolean'],
            [['name_id', 'name_en'], 'string', 'max' => 80],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_deleted' => 'Is Deleted',
            'name_id' => 'Name ID',
            'name_en' => 'Name En',
        ];
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empeducations".
 *
 * @property int $id
 * @property string $Description
 * @property int $EmpEducationTypeId
 * @property string|null $FinishDate
 * @property bool|null $IsDeleted
 * @property string|null $Name
 * @property string|null $StartDate
 * @property int|null $EmployeeId
 */
class Empeducations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empeducations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'Description', 'EmpEducationTypeId'], 'required'],
            [['id', 'EmpEducationTypeId', 'EmployeeId'], 'integer'],
            [['FinishDate', 'StartDate'], 'safe'],
            [['IsDeleted'], 'boolean'],
            [['Description'], 'string', 'max' => 200],
            [['Name'], 'string', 'max' => 120],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Description' => 'Description',
            'EmpEducationTypeId' => 'Emp Education Type ID',
            'FinishDate' => 'Finish Date',
            'IsDeleted' => 'Is Deleted',
            'Name' => 'Name',
            'StartDate' => 'Start Date',
            'EmployeeId' => 'Employee ID',
        ];
    }
}

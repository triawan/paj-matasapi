<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empeducationtypes".
 *
 * @property int $id
 * @property bool $IsDeleted
 * @property string $Name
 */
class Empeducationtypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empeducationtypes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'Name'], 'required'],
            [['id'], 'integer'],
            [['IsDeleted'], 'boolean'],
            [['Name'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'IsDeleted' => 'Is Deleted',
            'Name' => 'Name',
        ];
    }
}
